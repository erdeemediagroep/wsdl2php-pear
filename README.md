wsdl2php - PHP 5+ Web Service Proxy Generator
=============================================

wsdl2php is a tool for generating PHP 5+ proxy classes that can be used to access 
web services. It make use of both the SOAP and DOM extension.

This version is based on the release of version 0.2.1 (2007) from http://www.sf.net/projects/wsdl2php , adapted to run in PHP 7 and published on https://bitbucket.org/erdeemediagroep/wsdl2php-pear/.

Requirements
------------
In order to use wsdl2php you must fullfill the following requirements:

* PHP 5 or more (tested on PHP 7 too) with SOAP and DOM enabled
* PEAR installer >= 1.9.1


Building
--------

To create the PEAR archive:

    git clone git@bitbucket.org:erdeemediagroep/wsdl2php-pear.git
    cd wsdl2php-pear
    # Mac metadata will break the archive so on Macs `--no-mac-metadata` is required
    tar -czvf build/wsdl2php.tgz --no-mac-metadata --exclude build/ *


Installation
------------

You can install the latest version with:

    sudo pear install https://bitbucket.org/erdeemediagroep/wsdl2php-pear/raw/master/build/wsdl2php.tgz

Otherwise you can:

1. Build the archive or download the latest release from https://bitbucket.org/erdeemediagroep/wsdl2php-pear/downloads/
2. Install the package with the PEAR installer:

        sudo pear install path/to/wsdl2php.tgz


Credits
-------

* Knut Urdalen - author
* John Lindal - contributor
* Marc Kortleven - helper (PHP 7 changes)
